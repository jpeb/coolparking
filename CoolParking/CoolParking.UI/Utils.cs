﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.UI
{
    public static class Utils
    {
        public static void WriteColored(string str, ConsoleColor? fgColor = null, ConsoleColor? bgColor = null)
        {
            var (oldFg, oldBg) = (Console.ForegroundColor, Console.BackgroundColor);
            Console.ForegroundColor = fgColor ?? oldFg;
            Console.BackgroundColor = bgColor ?? oldBg;
            Console.Write(str);
            Console.ForegroundColor = oldFg;
            Console.BackgroundColor = oldBg;
        }
    }
}
