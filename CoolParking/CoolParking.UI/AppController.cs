﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.UI
{
    internal class AppController : IDisposable
    {
        private readonly ParkingService _parkingService;

        public AppController(string logFilePath)
        {
            _parkingService = new ParkingService(
                new TimerService(),
                new TimerService(),
                new LogService(logFilePath)
            );
        }

        public void Dispose()
        {
            _parkingService.Dispose();
        }

        // Вывести на экран текущий баланс Парковки.
        public void ShowCurrentParkingBalance()
        {
            var balance = _parkingService.GetBalance();

            Console.Write("\n► Текущий баланс парковки: ");
            Utils.WriteColored(balance.ToString(CultureInfo.InvariantCulture),
                ConsoleColor.Green);
        }

        // Вывести на экран сумму заработанных денег за текущий период (до записи в лог).
        public void ShowEarnedMoney()
        {
            var sum = _parkingService.GetLastParkingTransactions().Sum(t => t.Sum);

            Console.Write("\n► Сумма заработанных денег за текущий период (до записи в лог): ");
            Utils.WriteColored(sum.ToString(CultureInfo.InvariantCulture),
                ConsoleColor.Green);
        }

        // Вывести на экран количество свободных/занятых мест на парковке.
        public void ShowFreeOccupiedPlaces()
        {
            var freePlaces = _parkingService.GetFreePlaces();
            var occupiedPlaces = _parkingService.GetCapacity() - freePlaces;

            Console.Write("\n► Количество свободных мест на парковке: ");
            Utils.WriteColored(freePlaces.ToString(),
                ConsoleColor.Green);
            Console.Write("\n► Количество занятых мест на парковке: ");
            Utils.WriteColored(occupiedPlaces.ToString(),
                ConsoleColor.DarkRed);
        }

        // Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).
        public void ShowLastParkingTransactions()
        {
            if (_parkingService.LastParkingTransactions.Count > 0)
                Console.Write("\n► Транзакции за текущий период(до записи в лог): \n"
                              + string.Join("\n", _parkingService.LastParkingTransactions));
            else
                Utils.WriteColored("На данный момент транзакций нет\n", ConsoleColor.DarkRed);
        }

        // Вывести историю транзакций(считав данные из файла Transactions.log).
        public void ShowParkingTransactionsFromLog()
        {
            Console.Write("\n► История транзакций(из файла): \n"
                          + _parkingService.ReadFromLog());
        }

        // Вывести на экран список Тр.средств находящихся на Паркинге.
        public void ShowVehiclesInParking()
        {
            var vehicles = _parkingService.GetVehicles();

            if (vehicles.Count > 0)
                Console.Write("\n► Список транспортных средств на паркинге: \n"
                              + string.Join("\n", vehicles));
            else
                Utils.WriteColored("На данный момент на паркинге нет транспорта\n",
                    ConsoleColor.DarkRed);
        }

        // Поставить Тр.средство на Паркинг.
        public void AddVehicle()
        {
            Console.Write(
                "\n► Введите цифру, соответствующую типу вашего транспорта:\n1 - Легковое\n2 - Грузовое\n3 - Автобус\n4 - Мотоцикл\n");

            // VehicleType
            VehicleType? vehicleType;
            do
            {
                Console.Write("\n[1..4] > ");
                var keyInfo = Console.ReadKey();

                vehicleType = keyInfo.Key switch
                {
                    ConsoleKey.D1 or ConsoleKey.NumPad1 => VehicleType.PassengerCar,
                    ConsoleKey.D2 or ConsoleKey.NumPad2 => VehicleType.Truck,
                    ConsoleKey.D3 or ConsoleKey.NumPad3 => VehicleType.Bus,
                    ConsoleKey.D4 or ConsoleKey.NumPad4 => VehicleType.Motorcycle,
                    _ => null
                };
            } while (vehicleType is null);


            // Vehicle ID
            Console.Write(
                "\n\n► Введите ID машины формата ХХ-YYYY-XX,\nгде X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра: ");

            string id;
            bool idValid;
            do
            {
                idValid = true;
                Console.Write("\n > ");
                id = Console.ReadLine();

                if (id is null || !Regex.IsMatch(id, Settings.VehicleIdValidationPattern))
                {
                    idValid = false;
                    Utils.WriteColored("\nID не соответствует требуемому формату, попробуйте еще раз",
                        ConsoleColor.DarkRed);
                }
            } while (!idValid);


            // Balance
            Console.Write(
                "\n► Введите текущий баланс транспортного средства: ");


            decimal balance;
            bool balanceValid;
            do
            {
                Console.Write("\n > ");
                balanceValid = decimal.TryParse(Console.ReadLine(), out balance);

                if (!balanceValid)
                    Utils.WriteColored("\nОшибка. Введите целое или дробное положительное число",
                        ConsoleColor.DarkRed);
            } while (!balanceValid);


            var vehicle = new Vehicle(id, vehicleType.Value, balance);
            _parkingService.AddVehicle(vehicle);

            Utils.WriteColored("\nТранспортное средство успешно поставлено на парковку!",
                ConsoleColor.Green);
        }

        // Забрать транспортное средство с Паркинга.
        public void RemoveVehicleFromParking()
        {
            ShowVehiclesInParking();


            Console.Write(
                "\n\n► Введите ID машины(ХХ-YYYY-XX), которую нужно забрать: ");

            string id;
            bool idValid;
            do
            {
                idValid = true;
                Console.Write("\n > ");
                id = Console.ReadLine();

                if (id is null || !Regex.IsMatch(id, Settings.VehicleIdValidationPattern))
                {
                    idValid = false;
                    Utils.WriteColored("\nID не соответствует требуемому формату, попробуйте еще раз",
                        ConsoleColor.DarkRed);
                }
            } while (!idValid);


            _parkingService.RemoveVehicle(id);

            Utils.WriteColored("\nТранспортное средство успешно удалено с парковки!",
                ConsoleColor.Green);
        }

        // Пополнить баланс конкретного Тр. средства.
        public void TopUpVehicle()
        {
            ShowVehiclesInParking();

            // Id
            Console.Write(
                "\n\n► Введите ID машины(ХХ-YYYY-XX), баланс которой нужно пополнить: ");

            string id;
            bool idValid;
            do
            {
                idValid = true;
                Console.Write("\n > ");
                id = Console.ReadLine();

                if (id is null || !Regex.IsMatch(id, Settings.VehicleIdValidationPattern))
                {
                    idValid = false;
                    Utils.WriteColored("\nID не соответствует требуемому формату, попробуйте еще раз",
                        ConsoleColor.DarkRed);
                }
            } while (!idValid);


            // Sum
            Console.Write(
                "\n► Введите сумму пополнения: ");


            decimal sum;
            bool balanceValid;
            do
            {
                Console.Write("\n > ");
                balanceValid = decimal.TryParse(Console.ReadLine(), out sum);

                if (!balanceValid)
                    Utils.WriteColored("\nОшибка. Введите целое или дробное положительное число",
                        ConsoleColor.DarkRed);
            } while (!balanceValid);


            _parkingService.TopUpVehicle(id, sum);

            Utils.WriteColored("\nБаланс транспортного средства успешно пополнен!",
                ConsoleColor.Green);
        }
    }
}
