﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.UI
{
    internal class Program
    {
        private const string Title = @"
   _____            _   _____           _    _             
  / ____|          | | |  __ \         | |  (_)            
 | |     ___   ___ | | | |__) |_ _ _ __| | ___ _ __   __ _ 
 | |    / _ \ / _ \| | |  ___/ _` | '__| |/ / | '_ \ / _` |
 | |___| (_) | (_) | | | |  | (_| | |  |   <| | | | | (_| |
  \_____\___/ \___/|_| |_|   \__,_|_|  |_|\_\_|_| |_|\__, |
                                                      __/ |
   Use arrows and Enter for navigation...            |___/ 
   

";

        private static void Main(string[] args)
        {
            Console.Title = "CoolParking";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var app = new AppController(
                $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

            List<MenuOption> options = new()
            {
                new MenuOption("Вывести текущий баланс парковки", app.ShowCurrentParkingBalance),
                new MenuOption("Вывести сумму заработанных денег за текущий период", app.ShowEarnedMoney),
                new MenuOption("Вывести количество свободных/занятых мест на парковке", app.ShowFreeOccupiedPlaces),
                new MenuOption("Вывести на экран все транзакции парковки за текущий период",
                    app.ShowLastParkingTransactions),
                new MenuOption("Вывести историю транзакций из файла", app.ShowParkingTransactionsFromLog),
                new MenuOption("Вывести список транспортных средств находящихся на паркинге",
                    app.ShowVehiclesInParking),
                new MenuOption("Поставить транспортное средство на паркинг", app.AddVehicle),
                new MenuOption("Забрать транспортное средство с паркинга", app.RemoveVehicleFromParking),
                new MenuOption("Пополнить баланс конкретного транспортного средства", app.TopUpVehicle),
                new MenuOption("Выход", () =>
                {
                    app.Dispose();
                    Environment.Exit(0);
                })
            };

            var selectedIndex = 0;
            DrawMenu(options, options[selectedIndex]);

            while (true)
            {
                var keyInfo = Console.ReadKey();

                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                    {
                        selectedIndex = selectedIndex + 1 < options.Count ? selectedIndex + 1 : 0;
                        DrawMenu(options, options[selectedIndex]);
                        break;
                    }

                    case ConsoleKey.UpArrow:
                    {
                        selectedIndex = selectedIndex - 1 >= 0 ? selectedIndex - 1 : options.Count - 1;
                        DrawMenu(options, options[selectedIndex]);
                        break;
                    }

                    case ConsoleKey.Enter:
                    {
                        Console.Clear();

                        try
                        {
                            options[selectedIndex].Action.Invoke();
                        }
                        catch (Exception e)
                        {
                            Utils.WriteColored("\n\n            ОШИБКА            \n", ConsoleColor.Black,
                                ConsoleColor.DarkRed);
                            Utils.WriteColored(e.Message, ConsoleColor.DarkRed, ConsoleColor.Black);
                        }

                        Utils.WriteColored("\n\nPress any key to continue...", ConsoleColor.DarkGreen,
                            ConsoleColor.Black);
                        Console.ReadKey();
                        DrawMenu(options, options[selectedIndex]);
                        break;
                    }
                }
            }
        }

        private static void DrawMenu(List<MenuOption> options, MenuOption selected)
        {
            Console.Clear();
            Utils.WriteColored(Title, ConsoleColor.Red, ConsoleColor.Black);

            foreach (var option in options)
            {
                var isSelected = option == selected;
                Utils.WriteColored($"{(isSelected ? '>' : ' ')} {option.Name}\n",
                    isSelected ? ConsoleColor.Black : ConsoleColor.White,
                    isSelected ? ConsoleColor.DarkGreen : ConsoleColor.Black);
            }
        }
    }
}
