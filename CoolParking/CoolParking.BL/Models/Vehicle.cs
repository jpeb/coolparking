﻿using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Utils;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidateParams(id, balance);

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        private static void ValidateParams(string id, decimal balance)
        {
            if (!Regex.IsMatch(id, Settings.VehicleIdValidationPattern))
                throw new ArgumentException($"ID must match pattern '{Settings.VehicleIdValidationPattern}'",
                    nameof(id));

            if (balance < 0)
                throw new ArgumentException("Balance must be positive",
                    nameof(balance));
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return Generators.GetRandVehicleRegistrationPlate();
        }

        public override string ToString()
        {
            return $"[{Id}] - Type: {VehicleType}, Balance: {Balance}";
        }
    }
}
