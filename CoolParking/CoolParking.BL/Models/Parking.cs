﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;
        private decimal _balance;

        private Parking(decimal initialBalance, int capacity)
        {
            Vehicles = new List<Vehicle>();
            Balance = initialBalance;
            Capacity = capacity;
        }

        public decimal Balance
        {
            get => _balance;
            set
            {
                if (value < 0)
                    throw new ArgumentException("Parking balance must be positive", nameof(Balance));

                _balance = value;
            }
        }

        public IList<Vehicle> Vehicles { get; set; }
        public int Capacity { get; }

        public static Parking GetInstance()
        {
            return _instance ??=  new Parking(Settings.InitialParkingBalance, Settings.ParkingCapacity);
        }
    }
}
