﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0; // Начальный баланс паркинга
        public const int ParkingCapacity = 10; // Вместимость паркинга
        public const int PaymentChargePeriod = 5; // Период списания оплаты, секунд
        public const int LoggingPeriod = 60; // Период записи в лог
        public const decimal PenaltyRate = 2.5m; // Коэффициент штрафа

        // Формат ID
        // ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра)
        public const string VehicleIdValidationPattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

        // Тарифы в зависимости от транспортного средства
        public static readonly ReadOnlyDictionary<VehicleType, decimal> Tariffs
            = new(new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2},
                {VehicleType.Truck, 5},
                {VehicleType.Bus, 3.5m},
                {VehicleType.Motorcycle, 1}
            });
    }
}
