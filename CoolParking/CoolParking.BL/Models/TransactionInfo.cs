﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public TransactionInfo(DateTime dateTime, string vehicleId, decimal sum)
        {
            DateTime = dateTime;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public DateTime DateTime { get; init; }
        public string VehicleId { get; init; }
        public decimal Sum { get; init; }

        public override string ToString()
        {
            return $"[{DateTime}] - VehicleId: {VehicleId}, Sum: {Sum}";
        }
    }
}
