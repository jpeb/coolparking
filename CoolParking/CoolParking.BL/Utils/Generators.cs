﻿using System;
using System.Text;

namespace CoolParking.BL.Utils
{
    public static class Generators
    {
        private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly Random Random = new();

        public static int GetRandInt(int min, int max)
        {
            return Random.Next(min, max + 1);
        }

        public static char GetRandChar()
        {
            return Chars[Random.Next(Chars.Length)];
        }

        public static string GetRandVehicleRegistrationPlate()
        {
            var sb = new StringBuilder(10);
            sb.Append(GetRandChar());
            sb.Append(GetRandChar());
            sb.Append('-');
            sb.Append(GetRandInt(1000, 9999));
            sb.Append('-');
            sb.Append(GetRandChar());
            sb.Append(GetRandChar());
            return sb.ToString().ToUpper();
        }
    }
}
